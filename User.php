<?php
include ('DB.php');

class User {
	private $table = "usr_tbl";
	private $name;
	private $status;
	private $age;

	public function getUserData(){
		$sql = "SELECT * FROM $this->table";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		return $stmt->fetchAll();

	}

	public function setAll($name, $sts, $age){
		$this->name   = $name;
		$this->status = $sts;
		$this->age    = $age;
	}
	public function insertOnce(){
		$sql = "INSERT INTO `$this->table` (`name`, `status`, `age`) VALUES (:name, :sts, :age)";
		$stmt = DB::prepare($sql);
		$stmt->bindParam(':name', $this->name);
		$stmt->bindParam(':sts', $this->status);
		$stmt->bindParam(':age', $this->age);
		return $stmt->execute();
	}


}


?>